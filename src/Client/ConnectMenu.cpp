#include "ConnectMenu.h"
#include "MainMenu.h"
#include <iostream>

ConnectMenu::ConnectMenu(GameEngine* game)
	: GameState(game)
{

}

ConnectMenu::~ConnectMenu()
{

}

void ConnectMenu::Update()
{
	GameEngine* game = GameState::GetGame();

	HandleInput();
}

void ConnectMenu::Render()
{
	GameEngine* game = GameState::GetGame();

	sf::RenderWindow* MainWindow = game->MainWindow();

	// temporary code, will be removed later
	sf::CircleShape circle(50.0f);
	circle.setFillColor(sf::Color::Red);
	MainWindow->draw(circle);

	MainWindow->display();
}

void ConnectMenu::HandleInput()
{
	GameEngine* game = GameState::GetGame();

	sf::RenderWindow* mainWindow = game->MainWindow();
	sf::Event event;

	while (mainWindow->pollEvent(event))
	{
		switch (event.type)
		{
			case sf::Event::Closed:
				game->Quit();
			break;
			case sf::Event::KeyPressed:
			switch (event.key.code)
			{
				case sf::Keyboard::Escape:
					game->ChangeState(new MainMenu(game)); // temporary code, will be removed later
				break;
			}
			break;
		}
	}
}