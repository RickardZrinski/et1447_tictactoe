#include "GameEngine.h"
#include "GameState.h"
#include "MainMenu.h"
#include <crtdbg.h>

int main()
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	GameEngine* game = new GameEngine();

	// Initialize and run game
	game->Init();

	// deallocate game
	delete game;
	game = nullptr;

	return 0;
}