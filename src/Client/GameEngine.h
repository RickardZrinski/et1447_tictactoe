#ifndef GAMEENGINE_H
#define GAMEENGINE_H
#include "GameState.h"
#include "SFML/Graphics.hpp"

class GameState;

class GameEngine
{
	public:
		GameEngine();
		~GameEngine();

		// Initialize
		void Init();

		// Set active game state
		void ChangeState(GameState* state);

		sf::RenderWindow* MainWindow() const;

		// temporary: will be removed when we have a ResourceHandler
		sf::Font* GetFont() const;

		bool IsRunning();
		void Quit();

	private:
		GameState* mActiveGameState;
		sf::RenderWindow* mMainWindow;
		sf::Font* mFont; // temporary: will be removed when we have a ResourceHandler class
		bool mRunning;

		// Keeps the game running
		void GameLoop();
		
		// Calls corresponding functions in game state on top of the stack
		void Update();
		void Render();

		// Handles global input used regardless of game state
		void HandleInput();
};

#endif // GAMEENGINE_H