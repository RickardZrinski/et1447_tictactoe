#include "GameState.h"

GameState::GameState()
{
	mGame = nullptr;
}

GameState::GameState(GameEngine* game)
{
	mGame = game;
}

GameState::~GameState()
{

}

GameEngine* GameState::GetGame() const
{
	return mGame;
}