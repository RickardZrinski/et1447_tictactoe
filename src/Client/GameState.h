#ifndef GAMESTATE_H
#define GAMESTATE_H
#include "GameEngine.h"

class GameEngine;

class GameState
{
	public:
		GameState();
		GameState(GameEngine* game);
		virtual ~GameState();

		virtual void Update() = 0;
		virtual void Render() = 0;

		GameEngine* GetGame() const;

	private:
		GameEngine* mGame;

		virtual void HandleInput() = 0;
};

#endif // GAMESTATE_H