#include "GameEngine.h"
#include "NetworkHandler.h"
#include "MainMenu.h"
#include <iostream>

/*
	TODO and possible future improvements:
		- Instead of passing the pointer to GameEngine around at so many points, pass it to the GameState
		  constructor and assign it to a GameEngine* member variable. It can then be accessed by the GameState
		  subclasses through a get-function in GameState.
		- Fairly certain that when the active game state is changed (which is only done in the respective GameState's
		  Update-function) the Render-function of the new active game state will be called instantly after.
		  Mainly there are two (possible) issues with this:
			1. Render() will be called before Update() in the new game state which most likely isn't very good.
			2. The Render()-function of the old (replaced) game state will not be called after Update() one last time
			   before the game state is changed.	  
*/

GameEngine::GameEngine()
{
	mActiveGameState = nullptr;
	mMainWindow = nullptr;
	mFont = nullptr;
	mRunning = false;
}

GameEngine::~GameEngine()
{
	if (mActiveGameState)
	{
		delete mActiveGameState;
	}

	if (mMainWindow)
	{
		delete mMainWindow;
	}

	if (mFont)
	{
		delete mFont;
	}
}

void GameEngine::Init()
{
	// Loading resources such as fonts, images etc should be done in a ResourceHandler class. Such a class will be added later on.
	mFont = new sf::Font;
	mFont->loadFromFile("../../../assets/fonts/arial.ttf");

	mMainWindow = new sf::RenderWindow(sf::VideoMode(800, 600), "Tic Tac Toe");

	mActiveGameState = new MainMenu(this);
	mRunning = true;
	GameLoop();
}

void GameEngine::GameLoop()
{
	while (IsRunning() && mActiveGameState)
	{
		Update();
		Render();
	}
}

void GameEngine::Update()
{
	mActiveGameState->Update();
}

void GameEngine::Render()
{
	mActiveGameState->Render();
}

void GameEngine::ChangeState(GameState* state)
{
	mMainWindow->clear();

	// delete current active game state if one exists
	if (mActiveGameState)
	{
		delete mActiveGameState;
		mActiveGameState = nullptr;
	}

	// set active game state
	mActiveGameState = state;
}

sf::Font* GameEngine::GetFont() const
{
	return mFont;
}

sf::RenderWindow* GameEngine::MainWindow() const
{
	return mMainWindow;
}

bool GameEngine::IsRunning()
{
	return mRunning;
}

void GameEngine::Quit()
{
	mMainWindow->close();
	mRunning = false;
}