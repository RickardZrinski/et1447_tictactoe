#ifndef MAINMENU_H
#define MAINMENU_H
#include "GameState.h"
#include "ConnectMenu.h"

class MainMenu : public GameState
{
	public:
		MainMenu(GameEngine* game);
		virtual ~MainMenu();

		virtual void Update();
		virtual void Render();

	private:
		sf::Text* mTitle;

		virtual void HandleInput();
};

#endif // MAINMENU_H