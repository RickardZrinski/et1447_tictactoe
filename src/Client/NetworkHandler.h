#ifndef NETWORKHANDLER_H
#define NETWORKHANDLER_H
#include <WinSock2.h>
#include <sdkddkver.h> // needed?
#include <conio.h>     // needed?
#include <stdio.h>     // needed?
#include <Windows.h>
#include <string>

#pragma once
#pragma comment(lib, "Ws2_32.lib")
#define SCK_VERSION2 0x0202

class NetworkHandler
{
	public:
		NetworkHandler();
		~NetworkHandler();

		void Init();

		void CreateSocket(std::string id, int port, const char* ip);
		
		void CreateSocket(std::string id);
		
		void BindSocket(std::string id);
		
		void SocketListen(std::string id);
		
		void SocketAccept(std::string listenSocket, std::string acceptingSocket);

		// If mode isn't equal to zero non-blocking mode is enabled
		void SetSocketBlockingMode(std::string id, u_long mode);
		
		void SocketConnect(std::string id);
		
		SOCKET* GetSocket(std::string id);

		int linearSearch(std::string id);

	private:
		SOCKET** mSocket;
		SOCKADDR_IN** mSocketAddr;
		std::string* mSocketMapper;
		int mArraySize;
		int mArrayCap;
};

#endif