#include "MainMenu.h"
#include <iostream>

MainMenu::MainMenu(GameEngine* game)
	: GameState(game)
{
	sf::Font* font = GameState::GetGame()->GetFont();
	mTitle = new sf::Text("Tic Tac Toe", *font, 50);
	mTitle->setColor(sf::Color::White);
}

MainMenu::~MainMenu()
{
	delete mTitle;
}

void MainMenu::Update()
{
	GameEngine* game = GameState::GetGame();

	HandleInput();
}

void MainMenu::Render()
{
	GameEngine* game = GameState::GetGame();

	sf::RenderWindow* mainWindow = game->MainWindow();
	
	mainWindow->draw(*mTitle);

	mainWindow->display();
}

void MainMenu::HandleInput()
{
	GameEngine* game = GameState::GetGame();

	sf::RenderWindow* mainWindow = game->MainWindow();
	sf::Event event;

	while (mainWindow->pollEvent(event))
	{
		std::cout << "mainWindow->pollEvent(event)" << std::endl;
		switch (event.type)
		{
			case sf::Event::Closed:
				game->Quit();
			break;
			case sf::Event::KeyPressed:
				switch (event.key.code)
				{
					case sf::Keyboard::Escape:
						game->ChangeState(new ConnectMenu(game)); // temporary code, will be removed later
					break;
				}
			break;
		}
	}
}