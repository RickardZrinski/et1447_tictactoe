#ifndef CONNECTMENU_H
#define CONNECTMENU_H
#include "GameState.h"

class ConnectMenu : public GameState
{
	public:
		ConnectMenu(GameEngine* game);
		virtual ~ConnectMenu();

		virtual void Update();
		virtual void Render();

	private:
		virtual void HandleInput();
};

#endif // CONNECTMENU_H