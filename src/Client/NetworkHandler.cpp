#include "NetworkHandler.h"

/*
	TODO and possible future improvements:
		- Replace all "id" string parameters with integer parameters that will be used to access the sockets to avoid
		  needlessly searching through the array a bunch of times. The call to linearSearch will instead be done
		  outside of the class.
		- Possibly add a PacketHandler class
		- Add a function for sending packets (in PacketHandler if implemented)
		- Add functions for serializing and deserializing packets (in PacketHandler if implemented)
		- Implement a way to dynamically increase the array size, or just use the vector class instead.
		- Possibly handle member arrays differently by using STL's map (associative array) instead.
		- etc
*/

NetworkHandler::NetworkHandler()
{
	mArraySize = 0;
	mArrayCap = 3;
	mSocket = new SOCKET*[mArrayCap];
	mSocketAddr = new SOCKADDR_IN*[mArrayCap];
	mSocketMapper = new std::string[mArrayCap];
}

void NetworkHandler::Init()
{
	WSADATA wsaData;
	int status = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (status != 0)
	{
		printf("WSAStartup failed with error: %d\n", status);
	}
}

void NetworkHandler::CreateSocket(std::string id, int port, const char* ip)
{
	mSocketMapper[mArraySize] = id;

	mSocket[mArraySize] = new SOCKET;
	*mSocket[mArraySize] = socket(AF_INET, SOCK_STREAM, NULL);

	mSocketAddr[mArraySize] = new SOCKADDR_IN;
	mSocketAddr[mArraySize]->sin_addr.s_addr = inet_addr(ip);
	mSocketAddr[mArraySize]->sin_family = AF_INET;
	mSocketAddr[mArraySize]->sin_port = htons(port);

	mArraySize++;
}

void NetworkHandler::CreateSocket(std::string id)
{
	mSocketMapper[mArraySize] = id;

	mSocket[mArraySize] = new SOCKET;
	*mSocket[mArraySize] = INVALID_SOCKET;

	mArraySize++;
}

void NetworkHandler::BindSocket(std::string id)
{
	int pos = linearSearch(id);

	int bindResult = bind(*mSocket[pos], (sockaddr*)&mSocketAddr[pos], sizeof(sockaddr));
}

void NetworkHandler::SocketListen(std::string id)
{
	int pos = linearSearch(id);

	int listenResult = listen(*mSocket[pos], SOMAXCONN);
}

void NetworkHandler::SocketAccept(std::string listenSocket, std::string acceptingSocket)
{
	int posListen = linearSearch(listenSocket);
	int posAccepting = linearSearch(acceptingSocket);
	
	int socketAddrSize = sizeof(sockaddr);
	*mSocket[posAccepting] = accept(*mSocket[posListen], (SOCKADDR*)&mSocketAddr[posListen], &socketAddrSize);
}

void NetworkHandler::SetSocketBlockingMode(std::string id, u_long mode)
{
	int pos = linearSearch(id);

	ioctlsocket(*mSocket[pos], FIONBIO, &mode);
}

void NetworkHandler::SocketConnect(std::string id)
{
	int pos = linearSearch(id);
	SOCKADDR_IN* address = mSocketAddr[pos];

	int status = connect(*mSocket[pos], (SOCKADDR*)&*address, sizeof(*address));
	if (status != 0)
	{
		printf("Connect attempt failed: %d\n", WSAGetLastError());
	}
}

SOCKET* NetworkHandler::GetSocket(std::string id)
{
	int pos = linearSearch(id);
	
	return mSocket[pos];
}

int NetworkHandler::linearSearch(std::string id)
{
	int pos = -1;

	for (int i = 0; i < mArraySize && pos == -1; i++)
	{
		if (mSocketMapper[i] == id)
		{
			pos = i;
		}
	}

	return pos;
}